#include "Datum.hpp"

int main() {
    Datum datum(15, 10, 2023);
    std::cout << "Datum: " << datum.getDan() << "." << datum.getMjesec() << "." << datum.getGodina() << std::endl;

    datum.setDan(25);
    datum.setMjesec(11);
    datum.setGodina(2024);

    std::cout << "Novi datum: " << datum.getDan() << "." << datum.getMjesec() << "." << datum.getGodina() << std::endl;
    return 0;
}