#include <iostream>

class Datum{
private:
    int dan;
    int mjesec;
    int godina;

public:
    Datum(int dan, int mjesec, int godina);

    //set

    void setDan(int dan){
        if (dan >= 1 && dan <= 31){
            this->dan = dan;
        } else{
            std::cout << "Neispravan unos dana.";
        }
    }
    void setMjesec(int mjesec){
        if (mjesec >= 1 && mjesec <=12){
            this->mjesec = mjesec;
        } else{
            std::cout << "Neispravan unos mjeseca.";
        }
    }
    void setGodina(int godina){
        if (godina > 0){
            this->godina = godina;
        } else{
            std::cout << "Neispravan unos godine.";
        }
    }
    //get

    int getDan() const{
        return dan;
    }
    int getMjesec() const{
        return mjesec;
    }
    int getGodina() const{
        return godina;
    }

};
