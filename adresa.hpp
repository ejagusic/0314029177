#include <string>

class Adresa{
private:
    std::string ulica;
    int broj;
    std::string grad;

public:
    Adresa(std::string ulica, int broj, std::string grad);

    //set

    void setUlica(std::string ulica);
    void setBroj(int broj);
    void setGrad(std::string grad);

    //get

    std::string getUlica();
    int getBroj();
    std::string getGrad();
};

